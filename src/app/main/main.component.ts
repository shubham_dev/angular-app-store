import { Component, OnInit } from '@angular/core';
import { Httpservice } from '../http.service'

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css', '../app.component.css']
})
export class MainComponent implements OnInit {
  appData: any;
  totalItems: number = 0;
  currentPage: number = 0;
  skip: number = 0;
  limit: number = 20;
  updateApp: boolean = false;
  page: number = 0;

  constructor(
    private httpService: Httpservice
  ) { }

  ngOnInit() {
      this.loadAppData();
  }

  loadAppData() {
    this.httpService.getAppData(this.skip, this.limit, this.updateApp).subscribe(res => {
      this.appData = res.data;
      this.totalItems = res.total;
      if (this.updateApp) {
        this.updateApp = !this.updateApp;
      }
      console.log('app data', this.appData)
    });
  }

  pageChanged(event: any): void {
    this.page = event.page;
    this.skip = (this.page - 1)*this.limit;
    this.loadAppData();

  }

  udpateAppData() {
    this.updateApp = true;
    delete this.appData;
    this.totalItems = 0;
    this.loadAppData();
  }
}
