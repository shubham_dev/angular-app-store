import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/filter';
import { Httpservice } from '../http.service'

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css', '../app.component.css']
})
export class DetailsComponent implements OnInit {
  appdetails: any;

  constructor(
    private route: ActivatedRoute,
    private httpService: Httpservice
  ) { }

  ngOnInit() {
    let queryparamsObj = this.route.queryParams;
    queryparamsObj = queryparamsObj['value'];
    console.log(queryparamsObj);
    if (queryparamsObj['pkg']) {
      this.httpService.getPackageDetails(queryparamsObj['pkg']).subscribe(data => {
        console.log('getting the data', data);
        this.appdetails = data;
      })
    }

  }
}
