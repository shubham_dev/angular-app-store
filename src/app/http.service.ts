import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../environments/environment';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class Httpservice {

  baseUrl = environment.apiUrl;

  constructor(
    private http: Http
  ) { }

  getPackageDetails(name) {
    const url: string = `${this.baseUrl}v1/app/package?name=${name}`;
    return this.http.get(url).pipe(map(data => data.json()));
  }

  getAppData(skip = 0, limit = 20, update = false) {
    const url: string = `${this.baseUrl}v1/app/list?skip=${skip}&limit=${limit}&update=${update}`;
    return this.http.get(url).pipe(map(data => data.json()));
  }

}
